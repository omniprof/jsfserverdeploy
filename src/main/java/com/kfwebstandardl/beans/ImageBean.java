package com.kfwebstandardl.beans;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Bean that represents an image
 *
 * @author Ken Fogel
 */
@Named
@RequestScoped
public class ImageBean {

    private String imageName;
    private String bookTitle;

    public ImageBean() {
        
    }
    
    public ImageBean(String imageName, String bookTitle) {
        this.imageName = imageName;
        this.bookTitle = bookTitle;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

}
